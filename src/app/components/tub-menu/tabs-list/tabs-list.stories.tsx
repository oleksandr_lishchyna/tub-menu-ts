import { Meta } from "@storybook/react";
import TabsList from "./tabs-list";
import TabListItem from "./tab-list-item";

const meta: Meta = {
  title: "components/tab-menu/tabs-list",
  component: TabsList,
};

export const primary = () =>
  (
    <TabsList activeTabId="third-tab">
      <TabListItem tabId="first-tab">Tab One</TabListItem>
      <TabListItem tabId="second-tab">Tab Two</TabListItem>
      <TabListItem tabId="third-tab">Tab Three</TabListItem>
    </TabsList>
  );

export default meta;
