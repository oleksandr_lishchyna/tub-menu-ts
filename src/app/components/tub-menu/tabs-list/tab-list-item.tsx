import Button, { ButtonProps } from "../../button/button";

export interface TabListItemProps extends ButtonProps {
  isActive?: boolean;
  tabId: string;
}

const TabListItem = ({ isActive, tabId, ...props }: TabListItemProps) => {
  const variant = isActive ? "primary" : "secondary";

  return <Button {...props} variant={variant} role="tab" aria-selected={isActive} />;
};

export default TabListItem;
