import { ReactElement, Children, cloneElement } from "react";
import { TabListItemProps } from "./tab-list-item";

export interface TabsListProps {
  activeTabId?: string;
  children: ReactElement<TabListItemProps> | ReactElement<TabListItemProps>[];
}

const TabsList = ({ children, activeTabId }: TabsListProps) => (
  <div role="tablist">
    {Children.map(children, child =>
      cloneElement(child, {
        isActive: child.props.tabId === activeTabId,
      }))}
  </div>
);

export default TabsList;
