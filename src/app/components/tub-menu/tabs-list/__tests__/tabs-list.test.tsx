import { render, screen } from "@testing-library/react";
import TabsList from "../tabs-list";
import TabListItem from "../tab-list-item";

describe("TabsList", () => {
  beforeEach(() => {
    render(
      <TabsList activeTabId="first-tab">
        <TabListItem tabId="first-tab">First Tab</TabListItem>
        <TabListItem tabId="second-tab">Second Tab</TabListItem>
        <TabListItem tabId="third-tab">Third Tab</TabListItem>
      </TabsList>,
    );
  });

  it("should have `tablist` role", () => {
    expect(screen.getByRole("tablist")).toBeInTheDocument();
  });

  it("renders tab items correctly", () => {
    expect(screen.getAllByRole("tab")).toHaveLength(3);
  });

  it("sets active tab", () => {
    const activeTab = screen.getByRole("tab", {
      selected: true,
    });

    expect(activeTab).toHaveTextContent("First Tab");
    expect(activeTab).toHaveClass("primary");
  });
});
