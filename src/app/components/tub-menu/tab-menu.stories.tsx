import { Meta } from "@storybook/react";
import TabMenu from "./tab-menu";
import TabPanel from "./tab-panel/tab-panel";
import TabListItem from "./tabs-list/tab-list-item";
import TabsList from "./tabs-list/tabs-list";

const meta: Meta = {
  title: "components/tab-menu",
  component: TabMenu,
};

export const primary = () => (
  <TabMenu>
    <TabsList>
      <TabListItem tabId="tab-one">Tab One</TabListItem>
      <TabListItem tabId="tab-two">Tab Two</TabListItem>
    </TabsList>
    <TabPanel>Tab Content One</TabPanel>
    <TabPanel>Tab Content Two</TabPanel>
  </TabMenu>
);

export default meta;
