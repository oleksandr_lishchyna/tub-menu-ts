import React, { useState } from "react";
import { HTMLAttributes, ReactElement } from "react";
import TabsList, { TabsListProps } from "./tabs-list/tabs-list";
import TabPanel, { TabPanelProps } from "./tab-panel/tab-panel";
import TabListItem from "./tabs-list/tab-list-item";
import styles from "./tab-menu.module.scss";
import classNames from "classnames";

export interface TabMenuProps extends HTMLAttributes<HTMLDivElement> {
  children?: ReactElement<TabsListProps> | ReactElement<TabsListProps>[] | ReactElement<TabPanelProps> | ReactElement<TabPanelProps>[];
}

const TabMenu = ({ children, className, ...props }: TabMenuProps) => {
  const cn = classNames(styles.root, className);
  const [activeTab, setActiveTab] = useState("first-tab");
  const handleTab1 = () => {
    setActiveTab("first-tab");
  };
  const handleTab2 = () => {
    setActiveTab("second-tab");
  };
  const handleTab3 = () => {
    setActiveTab("third-tab");
  };
  return (
    <div>
      <div {...props} className={cn}>
        <div className="Tabs" >
          <div className={styles.tabtitle} ><h3>Навыки и опыт</h3></div>
          <div className={styles.tabdiscription}>
            <p>В данном разделе заполните, пожалуйста, ваши навыки и опыт. Мы подберем вакансии которые смогут вас заинтересовать. </p>
          </div>


          <TabsList activeTabId={activeTab}>
            <TabListItem tabId="first-tab" onClick={handleTab1}>Общие данные</TabListItem>
            <TabListItem tabId="second-tab" onClick={handleTab2}>Опыт работы</TabListItem>
            <TabListItem tabId="third-tab" onClick={handleTab3}>Знания языков</TabListItem>
          </TabsList>


          <div className="outlet">
            {activeTab === "first-tab" ? <TabPanel>Tab Content One</TabPanel> :
              activeTab === "second-tab" ? <TabPanel>Tab Content Two</TabPanel> :

                <TabPanel>Tab Content Three</TabPanel>

            }
          </div>
        </div>;
      </div>
    </div>
  )
}




export default TabMenu;


