import { HTMLAttributes } from "react";
import classNames from "classnames";
import styles from "./tab-panel.module.scss";

export interface TabPanelProps extends HTMLAttributes<HTMLDivElement> {
}

const TabPanel = ({ children, className, ...props }: TabPanelProps) => {
  const cn = classNames(className, styles.root);

  return <div {...props} className={cn} role="tabpanel">{children}</div>;
};

export default TabPanel;
