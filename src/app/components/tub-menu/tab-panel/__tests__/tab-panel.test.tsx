import { render, screen } from "@testing-library/react";
import TabPanel from "../tab-panel";

describe("TabPanel", () => {
  beforeEach(() => {
    render(
      <TabPanel>
        Some Content
      </TabPanel>,
    );
  });

  it("renders correctly", () => {
    expect(screen.getByText("Some Content")).toBeInTheDocument();
  });

  it("has correct aria-role", () => {
    expect(screen.getByRole("tabpanel")).toBeInTheDocument();
  });
});
