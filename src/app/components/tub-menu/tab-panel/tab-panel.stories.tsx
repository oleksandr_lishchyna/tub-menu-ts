import { Meta } from "@storybook/react";
import TabPanel from "./tab-panel";

const meta: Meta = {
  title: "components/tab-menu/tab-panel",
  component: TabPanel,
};

export const primary = () => <TabPanel>Some Content</TabPanel>;

export default meta;
