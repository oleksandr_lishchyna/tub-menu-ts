import { Meta, Story } from "@storybook/react";
import Button, { ButtonProps } from "./button";

const meta: Meta = {
  title: "components/button",
  component: Button,
  args: {
    children: "Click Me!",
  },
  argTypes: {
    onClick: { action: "clicked" },
    variant: {
      options: ["primary", "secondary"],
      control: { type: "radio" },
    },
  },
};

const Template: Story<ButtonProps> = args => <Button {...args} />;

export const primary = Template.bind({});

export default meta;
