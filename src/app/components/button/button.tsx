import React from "react";
import classNames from "classnames";
import styles from "./button.module.scss";

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  type?: "button" | "submit" | "reset";
  variant?: "primary" | "secondary";
}

const Button = ({
  variant = "primary",
  className,
  ...props
}: ButtonProps) => {
  const cn = classNames(styles.root, className, {
    [styles.primary]: variant === "primary",
    [styles.secondary]: variant === "secondary",
  });

  return <button type="button" {...props} className={cn} />;
};

export default Button;